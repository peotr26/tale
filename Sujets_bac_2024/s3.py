# Exercice 1

def maximum_tableau(tab: [int | float]) -> int | float:
    """Renvoie la valeur maximum du tableau."""
    assert len(tab) != 0
    maxi: int | float = tab[0]
    for e in tab:
        if e > maxi:
            maxi = e
    return maxi


assert maximum_tableau([98, 12, 104, 23, 131, 9])
assert maximum_tableau([-27, 24, -3, 15])

# Exercice 2


class Pile:
    """Classe définissant une structure de pile."""
    def __init__(self):
        self.contenu = []

    def est_vide(self) -> bool:
        """Renvoie un booléen indiquant si la pile est vide."""
        return self.contenu == []

    def empiler(self, v: str):
        """Place l'élément v au sommet de la pile"""
        self.contenu.append(v)

    def depiler(self) -> str:
        """
        Retire et renvoie l'élément placé au sommet de la pile,
        si la pile n’est pas vide. Produit une erreur sinon.
        """
        assert not self.est_vide()
        return self.contenu.pop()


def bon_parenthesage(ch: str) -> bool:
    """Renvoie un booléen indiquant si la chaîne ch
    est bien parenthésée"""
    p = Pile()
    for c in ch:
        if c == "(":
            p.empiler(c)
        elif c == ")":
            if p.est_vide():
                return False
            else:
                p.depiler()
    return p.est_vide()


assert bon_parenthesage("((()())(()))") is True
assert bon_parenthesage("())(()") is False
assert bon_parenthesage("(())(()") is False
