import time as tm

ex1 = [
    [4],
    [6, 2],
    [3, 5, 7],
    [5, 1, 6, 2],
    [4, 7, 3, 5, 2],
    [4, 5, 6, 4, 5, 4],
    [3, 4, 3, 3, 1, 9, 4],
    [3, 7, 8, 4, 4, 3, 2, 6],
    [8, 4, 6, 4, 9, 0, 4, 5, 5],
    [4, 5, 4, 5, 3, 2, 8, 7, 4, 8],
    [9, 9, 6, 2, 1, 1, 5, 4, 8, 9, 0],
    [4, 6, 3, 5, 2, 9, 8, 7, 4, 6, 5, 3],
    [5, 9, 8, 3, 3, 2, 8, 2, 5, 1, 9, 5, 3],
    [2, 3, 0, 9, 4, 8, 2, 0, 3, 9, 4, 8, 0, 3]
]


def score_max(i: int, j: int, p: [int]) -> int:
    if i == len(p) - 1:
        return p[len(p) - 1][j]
    else:
        return p[i][j] + max(
            score_max(i+1, j, p), score_max(i+1, j+1, p))


def pyramide_nulle(n: int) -> [int]:
    resultat = []
    for i in range(1, n+1):
        resultat.append([0 for k in range(i)])
    return resultat


def prog_dyn(p: [int]) -> int:
    n = len(p)
    s = pyramide_nulle(n)
    for j in range(n):
        s[n-1][j] = p[n-1][j]
    for i in range(n-2, -1, -1):
        for j in range(0, i+1):
            s[i][j] = p[i][j] + max(s[i+1][j], s[i+1][j+1])
    return s[0][0]


assert score_max(0, 0, ex1) == prog_dyn(ex1)

start = tm.time()
score_max(0, 0, ex1)
print(tm.time()-start)

start = tm.time()
prog_dyn(ex1)
print(tm.time()-start)
