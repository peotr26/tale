import turtle

n = 3
turtle.speed(0)
colour = 'red'
turtle.color(colour)


def segment(n: int, L: int):
    if n == 0:
        turtle.forward(L)
    else:
        segment(n-1, L/3)
        turtle.left(180/3)
        segment(n-1, L/3)
        turtle.left(180+180/3)
        segment(n-1, L/3)
        turtle.left(180/3)
        segment(n-1, L/3)


turtle.up()
turtle.goto(-150, 130)
turtle.down()
turtle.fillcolor(colour)
turtle.begin_fill()
for i in range(3):
    segment(n, 300)
    turtle.right((180/3)*2)
turtle.end_fill()
turtle.mainloop()
