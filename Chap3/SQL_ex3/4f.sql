SELECT Nom_auteur, Auteur.Nom_pays, Pays.Population
FROM Auteur
JOIN Pays
ON Pays.Nom_pays = Auteur.Nom_pays;