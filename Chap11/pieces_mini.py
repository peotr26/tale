import sys


sys.setrecursionlimit(1000000000)


def nb_mini_pieces(a_rendre: int) -> int:
    global pieces
    pieces = [2, 5, 10, 20, 50, 100, 200]
    stock = [0]*100000000
    return rec_mini_pieces(a_rendre, stock)


def rec_mini_pieces(a_rendre: int, stock: [int]) -> int:
    if a_rendre == 0:
        return 0
    elif stock[a_rendre] != 0:
        return stock[a_rendre]
    else:
        mini = 100000000  # a_rendre <= 1000
        for i in range(len(pieces)):
            if pieces[i] <= a_rendre:
                nb = 1 + rec_mini_pieces(a_rendre-pieces[i], stock)
                if nb < mini:
                    mini = nb
                    stock[a_rendre] = mini
        return mini


print(nb_mini_pieces(534897))
