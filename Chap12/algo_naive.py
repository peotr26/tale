def recherche(S: str, M: str) -> [int]:
    n, p = len(S), len(M)
    i, j = 0, 0
    tab = []
    while i < n - p + 1:
        j = 0
        while j < p and S[i+j] == M[j]:
            j += 1
        if j == p:
            tab.append(i)
            i += p
        else:
            i += 1
    return tab


S = "Hello, world!"*1000000
M = "ll"

print(recherche(S, M))
