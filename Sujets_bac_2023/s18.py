# Exercice 1

def max_et_indice(tab: list) -> tuple:
    '''
    Fonction qui renvoie le maximum du tableau tab et son indice.
    '''
    n: int = len(tab)
    maxi: int = tab[0]
    maxi_indice: int = 0
    for i in range(1, n):
        tmp = tab[i]
        if tmp > maxi:
            maxi = tmp
            maxi_indice = i
    return maxi, maxi_indice


assert max_et_indice([1, 5, 6, 9, 1, 2, 3, 7, 9, 8]) == (9, 3)
assert max_et_indice([-1, -1, 3, 3, 3]) == (3, 2)

# Exercice 2


def est_un_ordre(tab: list) -> bool:
    '''
    Renvoie True si tab est de longueur n et contient tous les
    entiers de 1 à n, False sinon
    '''
    for i in range(1, len(tab)):
        if tab[i] > len(tab)+1:
            return False
    return True


assert est_un_ordre([1, 6, 2, 8, 3, 7]) is False
assert est_un_ordre([5, 4, 3, 6, 7, 2, 1, 8, 9]) is True


def nombre_points_rupture(ordre: list) -> int:
    '''
    Renvoie le nombre de point de rupture de ordre qui représente
    un ordre de gènes de chromosome
    '''
    assert est_un_ordre(ordre) is True    # ordre n'est pas un ordre de gènes
    n = len(ordre)
    nb = 0
    if ordre[0] != 1:    # le premier n'est pas 1
        nb = nb + 1
    i = 0
    while i < n-1:
        if ordre[i+1]-ordre[i] not in [-1, 1]:    # l'écart n'est pas 1
            nb = nb + 1
        i = i + 1
    if ordre[n-1] != n:    # le dernier n'est pas n
        nb = nb + 1
    return nb


assert nombre_points_rupture([5, 4, 3, 6, 7, 2, 1, 8, 9]) == 4
assert nombre_points_rupture([1, 2, 3, 4, 5]) == 0
