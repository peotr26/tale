def rendu_monnaie_centimes(s_due, s_versee):
    pieces = [1, 2, 5, 10, 20, 50, 100, 200]
    rendu = []
    a_rendre = s_versee-s_due
    i = len(pieces) - 1
    while a_rendre > 0:
        if pieces[i] <= a_rendre:
            rendu.append(pieces[i])
            a_rendre = a_rendre - pieces[i]
        else:
            i = i-1
    return rendu


assert rendu_monnaie_centimes(700, 700) == []
assert rendu_monnaie_centimes(112, 500) == [200, 100, 50, 20, 10, 5, 2, 1]


pieces = [1, 2, 5, 10, 20, 50, 100, 200]


def rendu_monnaie_centimes_rec(a_rendre, solution=[], i=len(pieces)-1):
    if a_rendre == 0:
        return []
    p = pieces[i]
    if p <= a_rendre:
        print(p)
        solution.append(p)
        return rendu_monnaie_centimes_rec(a_rendre - p, solution, i)
    else:
        print(p)
        return rendu_monnaie_centimes_rec(a_rendre, solution, i-1)


print(rendu_monnaie_centimes_rec(68, [], 7))
print(rendu_monnaie_centimes_rec(291, [], 7))
