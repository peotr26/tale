# Exercice 1

class Arbre:
    def __init__(self, etiquette):
        self.v = etiquette
        self.fg = None
        self.fd = None


def taille(a) -> int:
    if a.v is None:
        return 0
    elif a.fg is None and a.fd is None:
        return 1
    elif a.fg is None:
        return 1 + taille(a.fd)
    elif a.fd is None:
        return 1 + taille(a.fg)
    else:
        return 1 + taille(a.fd) + taille(a.fg)


def hauteur(a) -> int:
    if a.v is None:
        return 0
    elif a.fg is None and a.fd is None:
        return 1
    elif a.fg is None:
        return 1 + hauteur(a.fd)
    elif a.fd is None:
        return 1 + hauteur(a.fg)
    else:
        fg = hauteur(a.fg)
        fd = hauteur(a.fd)
        if fg > fd:
            return 1 + fg
        else:
            return 1 + fd


a = Arbre(0)
a.fg = Arbre(1)
a.fg.fg = Arbre(3)
a.fd = Arbre(2)
a.fd.fg = Arbre(4)
a.fd.fg.fd = Arbre(6)
a.fd.fd = Arbre(5)

assert taille(a) == 7
assert hauteur(a) == 4

# Exercice 2


def ajoute(indice, element, liste):
    nbre_elts = len(liste)
    L = [0 for i in range(nbre_elts + 1)]
    if indice <= nbre_elts:
        for i in range(indice):
            L[i] = liste[i]
        L[i + 1] = element
        for i in range(indice + 1, nbre_elts + 1):
            L[i] = liste[i - 1]
    else:
        for i in range(nbre_elts):
            L[i] = liste[i]
        L[i + 1] = element
    return L


assert ajoute(1, 4, [7, 8, 9]) == [7, 4, 8, 9]
assert ajoute(3, 4, [7, 8, 9]) == [7, 8, 9, 4]
assert ajoute(4, 4, [7, 8, 9]) == [7, 8, 9, 4]
assert ajoute(2, 6, [7, 8, 9, 7]) == [7, 8, 6, 9, 7]
