def nb_occurrences(ch: str, c: str) -> int:
    '''Fonction classique comptant le nombre d'occurrence.'''
    compteur: int = 0
    for e in ch:
        if e == c:
            compteur += 1
    return compteur


def nb_occurrences_rec(ch: str, c: str) -> int:
    '''Fonction récursive comptant le nombre d'occurrence.'''
    if ch == '':
        return 0
    elif ch[0] == c:
        compteur = 1
    else:
        compteur = 0
    return compteur + nb_occurrences_rec(ch[1:], c)


teste: str = 'BONJOUR'
assert nb_occurrences(teste, 'O') == 2
assert nb_occurrences_rec(teste, 'O') == 2
