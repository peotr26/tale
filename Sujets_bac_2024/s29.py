# Exercice 1

def moyenne(notes: [float]) -> float:
    return sum([nt[0]*nt[1] for nt in notes]) / sum([nt[1] for nt in notes])


assert moyenne([(15.0, 2), (9.0, 1), (12.0, 3)]) == 12.5

# Exercice 2


def ligne_suivante(ligne):
    '''Renvoie la ligne suivant ligne du triangle de Pascal'''
    ligne_suiv = [1]
    for i in range(len(ligne)-1):
        ligne_suiv.append(ligne[i] + ligne[i+1])
    ligne_suiv.append(1)
    return ligne_suiv


def pascal(n):
    '''Renvoie le triangle de Pascal de hauteur n'''
    triangle = [[1]]
    for k in range(n):
        ligne_k = ligne_suivante(triangle[-1])
        triangle.append(ligne_k)
    return triangle


assert pascal(2) == [[1], [1, 1], [1, 2, 1]]
assert pascal(3) == [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1]]
