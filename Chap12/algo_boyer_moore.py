def traitement_motif(M: str) -> dict:
    d = {}
    p = len(M)
    for j in range(p):
        d[M[j]] = j
    return d


def decalage_suivant_motif(S: str, d: dict, i: int, j: int) -> int:
    if S[i+j] in d:
        i = i + max(1, j-d[S[i+j]])
    else:
        i = i + j + 1
    return i


def algo_BM(S: str, M: str) -> list:
    n = len(S)
    p = len(M)
    liste_indice = []
    dico = traitement_motif(M)
    i = 0
    while i < n - p + 1:
        j = p - 1
        while j > -1 and S[i+j] == M[j]:
            j = j - 1
        if j == -1:
            liste_indice.append(i)
            i = i + p
        else:
            i = decalage_suivant_motif(S, dico, i, j)
    return liste_indice
