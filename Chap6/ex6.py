from __future__ import annotations


class AdresseIP:
    def __init__(self, adresse: str):
        self.adresse = adresse

    def liste_octet(self) -> list:
        return [int(i) for i in self.adresse.split(".")]

    def est_reservee(self) -> bool:
        return self.liste_octet()[3] == 0 or self.liste_octet()[3] == 255

    def adresse_suivante(self) -> AdresseIP | bool:
        if self.liste_octet()[3] < 254:
            octet_nouveau = self.liste_octet()[3]+1
            return AdresseIP('192.168.0.'+str(octet_nouveau))
        else:
            return False


adresse1 = AdresseIP('192.168.0.1')
adresse2 = AdresseIP('192.168.0.2')
adresse3 = AdresseIP('192.168.0.0')

print(adresse1.est_reservee())
print(adresse3.est_reservee())
print(adresse2.adresse_suivante().adresse)
