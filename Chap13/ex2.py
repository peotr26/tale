def caesar(char_str: str, key: int) -> str:
    result = ""
    for char in char_str:
        result += chr((ord(char) - 65 + key) % 26 + 65)
    return result


char_str = "UNKXWQNDAZDNUZDRUBXRCJYYXACNJRAUDVRNANNCURKNACNMNVXDENVNWC"
assert caesar(char_str, -9) == "LEBONHEURQUELQUILSOITAPPORTEAIRLUMIEREETLIBERTEDEMOUVEMENT"
