def suite(n: int) -> int:
    if n == 0:
        return 0
    valeurs = [0, 2]
    for i in range(2, n+1):
        valeurs += [(2*valeurs[1]) - (3*valeurs.pop(0)) - 1]
    return valeurs[1]


def suite_dyn(n: int) -> int:
    valeurs = [0]*(n+1)
    if n == 0:
        return 0
    valeurs[1] = 2
    for i in range(2, n+1):
        valeurs[i] = (2*valeurs[i-1]) - (3*valeurs[i-2]) - 1
    return valeurs[n]


def suite_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 2
    else:
        return (2*suite_rec(n-1)) - (3*suite_rec(n-2)) - 1


def suite_rec_dyn(n: int) -> int:
    stock = [0]*(n+1)
    return res_suite(n, stock)


def res_suite(n: int, stock: [int]) -> int:
    if stock[n] != 0:
        return stock[n]
    elif n == 0:
        stock[n] = 0
        return stock[n]
    elif n == 1:
        stock[n] = 2
        return stock[n]
    else:
        stock[n] = (2*res_suite(n-1, stock)) - (3*res_suite(n-2, stock)) - 1
        return stock[n]


n = 5
assert suite(n) == suite_dyn(n) == suite_rec(n) == suite_rec_dyn(n) == -22
n = 0
assert suite(n) == suite_dyn(n) == suite_rec(n) == suite_rec_dyn(n) == 0
n = 1
assert suite(n) == suite_dyn(n) == suite_rec(n) == suite_rec_dyn(n) == 2
