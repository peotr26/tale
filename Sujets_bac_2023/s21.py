# Exercice 1

def delta(liste: list) -> list:
    resultat = [liste[0]]
    for i in range(1, len(liste)):
        resultat.append(liste[i]-liste[i-1])
    return resultat


assert delta([1000, 800, 802, 1000, 1003]) == [1000, -200, 2, 198, 3]
assert delta([42]) == [42]

# Exercice 2


class Noeud:
    '''
    Classe implémentant un noeud d'arbre binaire.
    '''
    def __init__(self, g, v, d):
        '''
        Un objet Noeud possède 3 attributs :
        - gauche : le sous-arbre gauche,
        - valeur : la valeur de l'étiquette,
        - droit : le sous-arbre droit.
        '''
        self.gauche = g
        self.valeur = v
        self.droit = d

    def __str__(self):
        '''
        Renvoie la représentation du noeud en chaîne de caractères.
        '''
        return str(self.valeur)

    def est_une_feuille(self):
        '''
        Renvoie True si et seulement si le noeud est une feuille.
        '''
        return self.gauche is None and self.droit is None


def expression_infixe(e):
    s = ''
    if e.gauche is not None:
        s = '(' + s + expression_infixe(e.gauche)
    s = s + str(e.valeur)
    if e.droit is not None:
        s = s + expression_infixe(e.droit) + ')'
    return s


e = Noeud(Noeud(Noeud(None, 3, None),
          '*', Noeud(Noeud(None, 8, None), '+', Noeud(None, 7, None))),
          '-', Noeud(Noeud(None, 2, None), '+', Noeud(None, 1, None)))

assert expression_infixe(e) == '((3*(8+7))-(2+1))'
