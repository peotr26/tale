def rendu_monnaie_centimes_iter(a_rendre):
    pieces = [1, 2, 5, 10, 20, 50, 100, 200]
    rendu = []
    i = -1
    while a_rendre > 0:
        if pieces[i] <= a_rendre:
            rendu.append(pieces[i])
            a_rendre = a_rendre - pieces[i]
        else:
            i -= 1
    return rendu


pieces = [1, 3, 6, 12, 24, 30]


def rendu_monnaie_centimes_rec(a_rendre, rendu=[], i=len(pieces)-1):
    if a_rendre == 0:
        return rendu
    elif pieces[i] <= a_rendre:
        rendu.append(pieces[i])
        return rendu_monnaie_centimes_rec(a_rendre-pieces[i], rendu, i)
    else:
        return rendu_monnaie_centimes_rec(a_rendre, rendu, i-1)


print(rendu_monnaie_centimes_iter(19))

print(rendu_monnaie_centimes_rec(49))
