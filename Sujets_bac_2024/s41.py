# Exercice 1

class Noeud:
    def __init__(self, etiquette, gauche, droit):
        self.v = etiquette
        self.gauche = gauche
        self.droit = droit


a = Noeud(1, Noeud(4, None, None), Noeud(0, None, Noeud(7, None, None)))


def hauteur(a) -> int:
    if a is None:
        return -1
    elif a.gauche is None and a.droit is None:
        return 0
    elif a.gauche is None:
        return hauteur(a.droit) + 1
    elif a.droit is None:
        return hauteur(a.gauche) + 1
    else:
        return max(hauteur(a.gauche), hauteur(a.droit)) + 1


assert hauteur(a) == 2


def taille(a) -> int:
    if a is None:
        return 0
    else:
        return 1 + taille(a.gauche) + taille(a.droit)


assert taille(a) == 4
