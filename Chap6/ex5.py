class Voiture:
    def __init__(self, m: str, c: str, p: str, v: int | float):
        self.marque = m
        self.couleur = c
        self.pilote = p
        self.vitesse = v

    def choix_pilote(self, nom: str):
        self.pilote = nom

    def accelerer(self, taux: int | float, duree: int):
        self.vitesse += taux*duree

    def afficher(self):
        print(
            'La voiture est de marque', self.marque,
            ', de couleur', self.couleur,
            ', conduite par', self.pilote,
            ' et roule à la vitesse de', self.vitesse, 'm/s.'
            )


voiture1 = Voiture('Broum', 'bleue', 'Dunan', 10)
voiture2 = Voiture('Vroum', 'verte', 'Dupon', 15)

voiture1.choix_pilote('Orion')

voiture2.accelerer(0.8, 10)

voiture1.afficher()
voiture2.afficher()
