# Libellé

Les micro-noyaux vont-ils révolutionner l'architecture des noyaux ?

# Introduction

Dans un système d'exploitation, un des composants les plus important est le noyau. Ce dernier est un logiciel dont le but est d'allouer et contrôler les ressources. La première architecture de noyau est monolithique, c'est-à-dire que tout les composants du noyau (pilotes, ordonnanceur, piles réseaux) sont intégrer dans un seul programme qui s’exécute en tant qu'unique processus à la racine du système. Or à partir, des années 80, une nouvelle architecture commence à apparaître et à être expérimenté, les micro-noyaux. Ainsi, nous nous demanderons si les micro-noyaux vont révolutionner l'architecture des noyaux.

# Corps

## Introduction des termes

Un noyau a comme rôle de servir de lien entre le matériel et les logiciels exécuté.
Parmi ces fonctions, un noyau alloue les ressources aux différents composants grâce à l'ordonnanceur ainsi que de les contrôler au travers des pilotes. Le noyau est le premier processus que le système d'initialisation va initialiser.
Dans un système d'exploitation, il existe plusieurs niveaux de droits.
Il y a la racine, c'est à dire la région ayant tout les droits et le *userland* qui consiste en différents niveaux ayant différents niveaux de droits.
Traditionnellement, tout les composants du noyau font partie d'un seul exécutable et d'un seul processus.
Ce processus est exécuté au niveau de la racine.
Le micro-noyau fait lui le choix d'avoir un exécutable ne contenant que le strict minimum nécessaire au fonctionnement du noyau qui s’exécute au niveau de la racine, et plusieurs exécutables (pilotes, piles réseaux) communiquant avec le processus à la racine dans le *userland*.
Un processus est un programme en execution à qui est alloué des ressources dont notamment un espace de la mémoire. Aucun autre processus ne peut accéder à ces ressources, et donc aux données d'un autre processus.
Mais, afin de permettre les différents processus d'échanger des informations, dont notamment avec le noyau qui alloue les ressources.
Ce système de communication entre processus est appelé l'Inter-Process Communication (IPC).

## Intérêts techniques des micro-noyaux

L'idée des micro-noyaux apparaît au moment, où les systèmes d'exploitations commencent à se complexifier notamment avec la nécessiter de contrôler plus de matériels, avec plus de logiciels et services à exécuter.
De ce fait, la structure non-modulaire du noyau monolithique est limité et ces noyaux deviennent plus dur à maintenir au fur et à mesure que la code source se complexifie.
La nature monolithique du noyau mène aussi à une stabilité plus faible par rapport à un micro-noyau.
En effet, dans un micro-noyau lorsqu'un pilote crash, ce dernier étant un processus à part, n'entraîne pas le crash du noyau.
À l'inverse, le noyau monolithique crash dans son entièreté dès qu'un de ces composants crash.
Les micro-noyaux se basent donc sur un principe d'abstraction où le noyau en lui-même n'est qu'un petit ensemble de fonctionnalités très simples et abstraites.
Les micro-noyaux sont donc une implémentation concrète du principe de "diviser pour mieux régner".

## Limitations techniques du micro-noyau

Si aujourd'hui la grande majorité des noyaux utilisés sont sois des noyaux monolithiques ou des noyaux hybrides, c'est que historiquement les micro-noyaux ont été impacté par une limitation technique majeur, le manque d'efficacité et la forte latence du système d'intercommunication, l'IPC.
En effet, dans un noyau monolithique, vue que toutes les composantes font partis du même processus, alors elles sont accès aux données et aux ressources des autres composants du noyau.
De ce fait, la latence dans ce type de noyau est faible car les ressources ne sont pas échangés au travers de l'IPC mais juste en obtenant l'adresse dans la mémoire ou l'adresse de la ressource.
À l'inverse, dans le micro-noyaux, la majorité des composantes du noyau sont divisés en plein de différents processus, et de cette décision architectural, les données ne peuvent être échangés que aux travers de l'IPC et les ressources demandé qu'au travers de l'IPC, ajoutant une latence dans beaucoup des cas.
Si, dans le cas, d'un noyau monolithique, l'optimisation de l'IPC n'est pas nécessaire pour améliorer les performances du noyau.
Pour un micro-noyau, les performances du noyau ne peuvent être améliorer sans optimiser l'IPC.
Dans la seconde génération de micro-noyaux, durant les années 90, la latence d'un appel système pour un noyau monolithique est d'environ 20 μs alors que pour un micro-noyau, cette latence est d'environ 230 μs, soit une latence dix fois plus élevé.
Cette perte d'efficacité, mène dans le pire des cas, à une perte de 66% des performances.

# Conclusion

