# Exercice 1

def ou_exclusif(a: [int], b: [int]) -> [int]:
    """
    Fonction qui retourne une liste de ou exclusif entre 2 listes de 0 et de 1.
    """
    assert len(a) == len(b)
    result = []
    for i in range(len(a)):
        result += [a[i] ^ b[i]]
    return result


a = [1, 0, 1, 0, 1, 1, 0, 1]
b = [0, 1, 1, 1, 0, 1, 0, 0]
c = [1, 1, 0, 1]
d = [0, 0, 1, 1]

assert ou_exclusif(a, b) == [1, 1, 0, 1, 1, 0, 0, 1]
assert ou_exclusif(c, d) == [1, 1, 1, 0]

# Exercice 2


class Carre:
    def __init__(self, liste: [int], n: int):
        self.ordre = n
        self.tableau = [[liste[i + j * n] for i in range(n)] for j in range(n)]

    def affiche(self):
        """Affiche un carré"""
        for i in range(self.ordre):
            print(self.tableau[i])

    def somme_ligne(self, i: int) -> int:
        """Calcule la somme des valeurs de la ligne i"""
        somme = 0
        for j in range(self.ordre):
            somme = somme + self.tableau[i][j]
        return somme

    def somme_col(self, j: int) -> int:
        """Calcule la somme des valeurs de la colonne j"""
        somme = 0
        for i in range(self.ordre):
            somme = somme + self.tableau[i][j]
        return somme

    def est_semimagique(self) -> bool:
        """Renvoie si la carré est magique ou non."""
        s = self.somme_ligne(0)
        # test de la somme de chaque ligne
        for i in range(1, self.ordre):
            if self.somme_ligne(i) != s:
                return False
        # test de la somme de chaque colonne
        for j in range(self.ordre):
            if self.somme_col(j) != s:
                return False
        return True


liste = (3, 4, 5, 4, 4, 4, 5, 4, 3)
c3 = Carre(liste, 3)
assert c3.est_semimagique() is True
