# Exercice 1

def indices_maxi(tab: list) -> tuple:
    n: int = len(tab)
    maxi_indice: list = [0]
    maxi_valeur: int = tab[0]
    for i in range(1, n-1):
        if tab[i] > maxi_valeur:
            maxi_indice = [i]
            maxi_valeur = tab[i]
        elif tab[i] == maxi_valeur:
            maxi_indice.append(i)
    return maxi_valeur, maxi_indice


assert indices_maxi([1, 5, 6, 9, 1, 2, 3, 7, 9, 8]) == (9, [3, 8])
assert indices_maxi([7]) == (7, [0])

# Exercice 2


def positif(pile: list) -> list:
    pile_1 = list(pile)
    pile_2 = list()
    while pile_1 != []:
        x = pile_1.pop()
        if x >= 0:
            pile_2.append(x)
    while pile_2 != []:
        x = pile_2.pop()
        pile_1.append(x)
    return pile_1


assert positif([-1, 0, 5, -3, 4, -6, 10, 9, -8]) == [0, 5, 4, 10, 9]
assert positif([-2]) == []
