def creer_liste_vide() -> list:
    '''Créer une liste vide.'''
    return list()


def est_vide(L: list) -> bool:
    '''Vérifie si la liste L est vide ou non.'''
    return L == []


def car(L: list) -> int | float | str:
    '''Retourne la dernière valeur de la liste L.'''
    return L[-1]


def cdr(L: list) -> list:
    '''Retourne la liste L sans la dernière valeur.'''
    return L[:-1]


def cons(L: list, e: int | float | str) -> list:
    '''Ajoute à la liste L la valeur e.'''
    return L+[e]


def nb_elements(L: list) -> int:
    '''Compte le nombre d'élément de la liste L.'''
    compteur: int = 0
    while not est_vide(L):
        L = cdr(L)
        compteur += 1
    return compteur


def recherche(L: list, e: int | float | str) -> bool:
    '''Recherche l'élément e dans la liste L.'''
    while not est_vide(L):
        if car(L) == e:
            return True
        else:
            L = cdr(L)
    return False
