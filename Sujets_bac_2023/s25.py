# Exercice 1

def enumere(L: list) -> dict:
    """Renvoie les indices des différentes valeurs d'une liste."""
    resultats = {}
    for i in range(0, len(L)):
        if L[i] in resultats.keys():
            resultats[L[i]].append(i)
        else:
            resultats[L[i]] = [i]
    return resultats


assert enumere([1, 1, 2, 3, 2, 1]) == {1: [0, 1, 5], 2: [2, 4], 3: [3]}
assert enumere([1, 5, 8, 3, 4, 1, 5]) == {
    1: [0, 5], 5: [1, 6], 8: [2], 3: [3], 4: [4]}


# Exercice 2


class Arbre:
    def __init__(self, etiquette):
        self.v = etiquette
        self.fg = None
        self.fd = None


def parcours(arbre, liste: list) -> list:
    if arbre is not None:
        parcours(arbre.fg, liste)
        liste.append(arbre.v)
        parcours(arbre.fd, liste)
    return liste

# Fonction à compléter


def insere(arbre, cle):
    """ arbre est une instance de la classe Arbre qui implémente
    un arbre binaire de recherche.
    """
    if cle < arbre.v:
        if arbre.fg is not None:
            insere(arbre.fg, cle)
        else:
            arbre.fg = Arbre(cle)
    else:
        if arbre.fd is not None:
            insere(arbre.fd, cle)
        else:
            arbre.fd = Arbre(cle)


# Initialisation de l'arbre de recherche
arbre_recherche = Arbre(5)
insere(arbre_recherche, 2)
insere(arbre_recherche, 7)
insere(arbre_recherche, 3)

# Insertion de 1, 2, 6, et 8
insere(arbre_recherche, 1)
insere(arbre_recherche, 4)
insere(arbre_recherche, 6)
insere(arbre_recherche, 8)

# Test
assert parcours(arbre_recherche, []) == [1, 2, 3, 4, 5, 6, 7, 8]
