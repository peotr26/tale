# Sujet 1 : Évolution des IPC de micro-noyaux

## Liens

- https://cdworkshop.eit.lth.se/fileadmin/eit/project/142/IPC_Report.pdf
- https://trustworthy.systems/publications/nicta_full_text/8988.pdf
- https://u.cs.biu.ac.il/~wisemay/2os/microkernels/scheuermann.pdf
- https://www.usenix.org/system/files/atc20-gu.pdf
