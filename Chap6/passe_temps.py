import tkinter as tk
import random as rand

pixel_size = 4
resolution = [100, 100]


class Pixel:
    def __init__(self, pos: [int, int]):
        self.pos = pos
        self.colour = '#000000'
        self.pixel = canvas.create_rectangle(pos[0],
                                             pos[1],
                                             pos[0]+pixel_size,
                                             pos[1]+pixel_size,
                                             fill=self.colour)

    def change_colour(self, colour: str):
        self.colour = colour
        canvas.itemconfig(self.pixel, fill=self.colour)


class Screen:
    def __init__(self, resolution: [int, int]):
        self.pixels = []
        self.resolution = resolution
        for x in range(len(resolution)):
            print('x')
            self.pixels.append([])
            for y in range(len(resolution)):
                print('y')
                self.pixels[x].append(Pixel([x, y]))

    def change_random_pixel(self):
        x, y = self.resolution[0]
        x = rand.randint(0, x)
        y = rand.randint(0, y)
        self.pixels[x][y].change_colour()


win = tk.Tk()
canvas = tk.Canvas(win,
                   width=(resolution[0]*pixel_size),
                   height=(resolution[1]*pixel_size),
                   bg='#ffffff')
canvas.pack(side=tk.BOTTOM)

screen = Screen(resolution)

win.mainloop()
