# Exercice 3

1. Fonction `decimal`
    1. La fonction `decimal` renvoie une liste de la valeur ASCII de chaque caractère de la chaîne de caractère `phrase`.
    2. Non, cette fonction ne chiffre pas, car il n'existe pas de clé dans le but de décripter la liste. Il suffit juste de créer une fonction renvoie les caractères en fonction de la valeur ASCII.

2. Fonction `texte`

```python
def texte(liste: list) -> str:
    resultat = ""
    for char in liste:
        resultat += str(chr(char))
    return resultat
```

3. Fonctions `liste_chiffree` et `liste_dechiffree`
    1. $77 = 1001101_2$ et $67 = 1000011_2$ donc $1001101_2$ XOR $1000011_2 = 0001110_2 = 14$
    2. Fonction `liste_chiffree`

```python
def liste_chiffree(phrase: str, cle: str) -> list:
    resultat = []
    liste_decimal = (decimal(phrase), decimal(cle))
    for i in range(len(liste_decimal[0])):
        resultat.append(liste_decimal[0][i] ^ liste_decimal[1][i])
    return resultat
```
3. …
    3. `liste = liste_chiffree("Diviser pour régner", "chiffrement symétrique")` alors `liste == [39, 1, 31, 15, 21, 23, 23, 77, 21, 1, 1, 82, 83, 11, 132, 142, 26, 23, 27]`
    4. `texte(liste)` renvoie `'MRS`
    5. Pour `b^c = a` : `0^0 = 0`, `1^1 = 0`, `0^1 = 1`, `1^0 = 1` ; et pour `c^a = b` : `0^0 = 0`, `1^0 = 1`, `1^1 = 0`, `0^1 = 1`
    6. Fonction `liste_chiffree`

```python
def liste_dechiffree(liste: list, cle: str) -> str:
    resultat = []
    cle_decimal = decimal(cle)
    for i in range(len(liste)):
        resultat.append(cle_decimal[i] ^ liste[i])
    return texte(resultat)
```

4. Fonctions `liste_chiffree` et `liste_dechiffree`

```python
def liste_chiffree(phrase: str, cle: str) -> list:
    resultat = []
    cle_etendu = cle * (len(phrase) // len(cle))
    for i in range(len(phrase) - len(cle)):
        cle_etendu += cle[i % len(cle)]
    liste_decimal = (decimal(phrase), decimal(cle_etendu))
    for i in range(len(liste_decimal[0])):
        resultat.append(liste_decimal[0][i] ^ liste_decimal[1][i])
    return resultat


def liste_dechiffree(liste: list, cle: str) -> str:
    resultat = []
    cle_etendu = cle * (len(phrase) // len(cle))
    for i in range(len(phrase) - len(cle)):
        cle_etendu += cle[i % len(cle)]
    cle_decimal = decimal(cle_etendu)
    for i in range(len(liste)):
        resultat.append(cle_decimal[i] ^ liste[i])
    return texte(resultat)
```