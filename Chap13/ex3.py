def decimal(phrase: str) -> list:
    return [ord(c) for c in phrase]


def texte(liste: list) -> str:
    resultat = ""
    for char in liste:
        resultat += str(chr(char))
    return resultat


def liste_chiffree(phrase: str, cle: str) -> list:
    resultat = []
    cle_etendu = cle * (len(phrase) // len(cle))
    for i in range(len(phrase) - len(cle)):
        cle_etendu += cle[i % len(cle)]
    liste_decimal = (decimal(phrase), decimal(cle_etendu))
    for i in range(len(liste_decimal[0])):
        resultat.append(liste_decimal[0][i] ^ liste_decimal[1][i])
    return resultat


def liste_dechiffree(liste: list, cle: str) -> str:
    resultat = []
    cle_etendu = cle * (len(phrase) // len(cle))
    for i in range(len(phrase) - len(cle)):
        cle_etendu += cle[i % len(cle)]
    cle_decimal = decimal(cle_etendu)
    for i in range(len(liste)):
        resultat.append(cle_decimal[i] ^ liste[i])
    return texte(resultat)


phrase = "Diviser pour régner"
liste = liste_chiffree(phrase, "key")
print(liste_dechiffree(liste, "key"))
