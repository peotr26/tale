# Exercice 1

def tri(tab: list) -> list:
    n: int = len(tab)
    for k in range(0, n-1):
        maxi: int = k
        for i in range(k+1, n):
            if tab[i] > tab[maxi]:
                maxi = i
            tab.append(tab.pop(maxi))
    return tab


def fusion(tab1: list, tab2: list) -> list:
    tab: list = tab1 + tab2
    tab: list = tri(tab)
    return tab


tab1: list = [1, 4, 2, 6]
tab2: list = [3, 7]

tmp: list = fusion(tab1, tab2)

assert tmp == [1, 2, 3, 4, 6, 7]

# Correction - Exercice 1


def fusion_corr(tab1: list, tab2: list) -> list:
    '''
    Fonction qui fusionne deux listes triés de façon croissante.
    '''
    tab1_c = list(tab1)
    tab2_c = list(tab2)
    tab3 = list()
    while tab1_c != [] and tab2_c != []:
        if tab1_c[0] > tab2_c[0]:
            tab3.append(tab2_c.pop(0))
        else:
            tab3.append(tab1_c.pop(0))
    if tab1_c == []:
        tab3 += tab2_c
    else:
        tab3 += tab1_c
    return tab3


tab1: list = [1, 2, 6]
tab2: list = [3, 4, 5, 7, 8]

tmp: list = fusion_corr(tab1, tab2)

assert tmp == [1, 2, 3, 4, 5, 6, 7, 8]

# Exercice 2

romains = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}


def traduire_romain(nombre: str) -> int:
    '''
    Renvoie l’écriture décimale du nombre donné en chiffres
    romains
    '''
    if len(nombre) == 1:
        return romains[nombre[0]]
    elif romains[nombre[0]] >= romains[nombre[1]]:
        return romains[nombre[0]] + traduire_romain(nombre[1:])
    else:
        return romains[nombre[1]] - romains[nombre[0]]


print(traduire_romain('XIV'))
