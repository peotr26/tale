def somme(n: int) -> int:
    '''Fonction classique de la somme.'''
    s = 0
    for i in range(1, n+1):
        s += i
    return s


assert somme(3) == 6
assert somme(6) == 21


def somme_formule(n: int) -> int:
    '''Fonction avec la formule de la somme.'''
    return n*(n+1)//2


assert somme_formule(20) == somme(20)
assert somme_formule(33) == somme(33)


def somme_recursive(n: int) -> int:
    '''Fonction récursive de la somme.'''
    if n == 0:
        return 0
    else:
        return somme_recursive(n-1)+n


assert somme_recursive(20) == somme_formule(20)
assert somme_recursive(33) == somme_formule(33)
