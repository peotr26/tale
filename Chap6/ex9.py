class Pile:
    def __init__(self):
        self.pile = []

    def est_vide(self):
        return len(self.pile) == 0

    def empiler(self, e: int):
        self.pile += [e]

    def depiler(self) -> int:
        return self.pile.pop()

    def nb_elements(self) -> int:
        compteur: int = 0
        Q: list = Pile()
        while not self.est_vide():
            Q.empiler(self.depiler())
            compteur += 1
        while not Q.est_vide():
            self.empiler(Q.depiler())
        return compteur

    def afficher(self):
        Q: list = Pile()
        while not self.est_vide():
            Q.empiler(self.depiler())
        while not Q.est_vide():
            tmp = Q.depiler()
            print(tmp, end=' ')
            self.empiler(tmp)


# 1.a

print('Ex 1.a')

pile1 = Pile()
pile1.empiler(7)
pile1.empiler(5)
pile1.empiler(2)
pile1.afficher()

# 1.b

print('\n\nEx 1.b')

element1 = pile1.depiler()
pile1.empiler(5)
pile1.empiler(element1)
pile1.afficher()

# 2


def mystere(pile, element):
    pile2 = Pile()
    nb_elements = pile.nb_elements()
    for i in range(nb_elements):
        elem = pile.depiler()
        pile2.empiler(elem)
        if elem == element:
            return pile2
    return pile2


print('\n\nEx 2.a')

mystere(pile1, 2).afficher()
mystere(pile1, 5).afficher()
mystere(pile1, 7).afficher()

# La fonction mystère renvoie la pile jusqu'à l'élément.

# Ex 3

print('\n\nEx 3')


def etendre(pile1, pile2):
    while not pile2.est_vide():
        pile1.empiler(pile2.depiler())


pile2 = Pile()
pile2.empiler(1)
pile2.empiler(3)
pile2.empiler(4)

pile1.empiler(7)
pile1.empiler(5)
pile1.empiler(2)
pile1.empiler(3)

etendre(pile1, pile2)
pile1.afficher()

# Ex 4

print('\n\nEx 4')


def supprime_toutes_occurences(pile, element):
    Q = Pile()
    while not pile.est_vide():
        tmp = pile.depiler()
        if tmp != element:
            Q.empiler(tmp)
    while not Q.est_vide():
        pile.empiler(Q.depiler())


supprime_toutes_occurences(pile1, 3)
pile1.afficher()
print()
