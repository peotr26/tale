# Exercice 1

def moyenne(liste_notes: [(float, int)]) -> int | float:
    total_notes = 0
    total_coef = 0
    for valeurs in liste_notes:
        note, coef = valeurs
        total_notes += note * coef
        total_coef += coef
    return round(total_notes/total_coef, 2)


assert moyenne([(15, 2), (9, 1), (12, 3)]) == 12.5

# Exercice 2


def pascal(n):
    triangle = [[1]]
    for k in range(1, n+1):
        ligne_k = [triangle[k-1][0]]
        for i in range(1, k):
            ligne_k.append(triangle[k-1][i-1] + triangle[k-1][i])
        ligne_k.append(triangle[k-1][0])
        triangle.append(ligne_k)
    return triangle


assert pascal(4) == [[1], [1, 1], [1, 2, 1],
                     [1, 3, 3, 1], [1, 4, 6, 4, 1]]
assert pascal(5) == [[1], [1, 1], [1, 2, 1], [1, 3, 3, 1],
                     [1, 4, 6, 4, 1], [1, 5, 10, 10, 5, 1]]
