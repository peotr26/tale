def creer_file_vide():
    '''Créer une file vide.'''
    return []


def est_vide(F: list) -> bool:
    '''Vérifie si la file est vide ou non.'''
    return F == []


def enfiler(F: list, e: int | float | str) -> list:
    '''Enfile la valeur e dans la file F.'''
    return [e]+F


def defiler(F: list) -> int | float | str:
    '''Défile la première valeur de la file F.'''
    return F.pop()


def taille(F: list) -> int:
    '''Compte le nombre d'éléments de la file F.'''
    Q: list = creer_file_vide()
    compteur: int = 0
    while not est_vide(F):
        enfiler(Q, defiler(F))
        compteur += 1
    while not est_vide(Q):
        enfiler(F, defiler(Q))
    return compteur


def recherche(F: list, e: int | float | str) -> bool:
    '''Recherche la valeur e dans la file F.'''
    Q: list = creer_file_vide()
    etat: bool = False
    while not est_vide(F):
        tmp = defiler(F)
        Q = enfiler(Q, tmp)
        if tmp == e:
            etat = True
    print(Q)
    while not est_vide(Q):
        F = enfiler(F, defiler(Q))
    print(Q, F)
    return etat, F

