from random import randint

# Exercice 1


def tri_selection(T: list):
    n = len(T)
    for i in range(0, n-1):
        indice_mini = i
        for j in range(i+1, n):
            if T[j] < T[indice_mini]:
                indice_mini = j
        tmp = T[i]
        T[i] = T[indice_mini]
        T[indice_mini] = tmp


tab = [1, 52, 6, -9, 12]
tri_selection(tab)
assert tab == [-9, 1, 6, 12, 52]

# Exercice 2


def plus_ou_moins():
    nb_mystere = randint(1, 99)
    nb_test = int(input("Proposez un nombre entre 1 et 99 : "))
    compteur = 1
    while nb_mystere != nb_test and compteur < 10:
        compteur = compteur + 1
        if nb_mystere > nb_test:
            nb_test = int(input("Trop petit ! Testez encore : "))
        else:
            nb_test = int(input("Trop grand ! Testez encore : "))
    if nb_mystere == nb_test:
        print("Bravo ! Le nombre était ", nb_mystere)
        print("Nombre d'essais: ", compteur)
    else:
        print("Perdu ! Le nombre était ", nb_mystere)


plus_ou_moins()
