import random

# Exercice 1


def lancer(n: int) -> list:
    '''Simule un lancer de n dé·s'''
    resultat: list = []
    for i in range(n):
        resultat.append(random.randint(1, 6))
    return resultat


def paire_6(tab: list) -> bool:
    '''Vérifie si la liste tab contient plus de 2 fois la valeur 6.'''
    nb_6: int = 0
    for e in tab:
        if e == 6:
            nb_6 += 1
    if nb_6 >= 2:
        return True
    return False


assert len(lancer(5)) == 5
assert paire_6([3, 2, 6, 5, 6, 7])

# Exercice 2


def nbLig(image: list) -> int:
    '''renvoie le nombre de lignes de l'image'''
    return len(image)


def nbCol(image: list) -> int:
    '''renvoie la largeur de l'image'''
    return len(image[0])


def negatif(image: list) -> list:
    '''renvoie le négatif de l'image sous la forme
       d'une liste de listes'''

    # on cree une image de 0 aux mêmes dimensions que le paramètre image
    L = [[0 for k in range(nbCol(image))] for i in range(nbLig(image))]

    for i in range(nbLig(image)):
        for j in range(nbCol(image)):
            L[i][j] = 255-image[i][j]
    return L


def binaire(image: list, seuil: int) -> list:
    '''renvoie une image binarisee de l'image sous la forme
       d'une liste de listes contenant des 0 si la valeur
       du pixel est strictement inférieure au seuil
       et 1 sinon'''

    # on cree une image de 0 aux memes dimensions que le parametre image
    L = [[0 for k in range(nbCol(image))] for i in range(nbLig(image))]

    for i in range(nbLig(image)):
        for j in range(nbCol(image)):
            if image[i][j] < seuil:
                L[i][j] = 0
            else:
                L[i][j] = 1
    return L


img = [
    [20, 34, 254, 145, 6],
    [23, 124, 237, 225, 69],
    [197, 174, 207, 25, 87],
    [255, 0, 24, 197, 189]
]

assert nbLig(img) == 4
assert nbCol(img) == 5

assert negatif(img) == [
    [235, 221, 1, 110, 249],
    [232, 131, 18, 30, 186],
    [58, 81, 48, 230, 168],
    [0, 255, 231, 58, 66]
]

assert binaire(img, 120) == [
    [0, 0, 1, 1, 0],
    [0, 1, 1, 1, 0],
    [1, 1, 1, 0, 0],
    [1, 0, 0, 1, 1]
]
