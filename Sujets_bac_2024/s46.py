# Exercice 1

def recherche(tab: [int], n: int) -> int:
    n = len(tab)
    gauche = 0
    droite = n-1
    while gauche <= droite:
        milieu = (gauche+droite)//2
        if tab[milieu] == n:
            return milieu
        elif tab[milieu] < n:
            gauche = milieu+1
        else:
            droite = milieu-1
    return None


assert recherche([2, 3, 4, 5, 6], 5) == 3
assert recherche([2, 3, 4, 6, 7], 5) is None

# Exercice 2

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def position_alphabet(lettre):
    '''Renvoie la position de la lettre dans l'alphabet'''
    return ord(lettre) - ord('A')


def cesar(message, decalage):
    '''Renvoie le message codé par la méthode de César
    pour le decalage donné'''
    resultat = ''
    for c in message:
        if 'A' <= c and c <= 'Z':
            indice = (position_alphabet(c) + decalage) % 26
            resultat = resultat + alphabet[indice]
        else:
            resultat = resultat + c
    return resultat


assert cesar(
    'BONJOUR A TOUS. VIVE LA MATIERE NSI !', 4
    ) == 'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
assert cesar(
    'GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !', -5
    ) == 'BONJOUR A TOUS. VIVE LA MATIERE NSI !'
