import tkinter as tk
import random as rand
import math

###################################
#  TP sur la programmation objet  #
###################################

# Vous devrez, à partir de ce programme, ajouter les fonctionnalités
# ci-dessous :
#    * ajouter une méthode dans la classe Balle qui gère les rebonds sur les
#    bords ;
#    * ajouter plusieurs balles ;
#    * ajouter une méthode dans la classe Balle qui gère les rebonds entre
#    différentes balles ;
#    * provoquer des changements de couleur si rebond ;
#    * faire intervenir de l'aléatoire ;
#    * ...

L, H = 1024, 768
liste_coul = [
    '#CC0000',  # Rouge
    '#12853F',  # Vert
]
# Liste des coordonnées disponibles aux balles.
# Cela permet d'éviter les collisions au lancement.
coord_depart = [
    [175, 175],
    [185, H-185],
    [L/2, H/2],
    [L-200, 200],
    [L-165, H-165]
]
# Liste des vitesses parmi lesquels pourront choisir les balles.
vitesses = [
    -1,
    1
]


class Balle:
    """
    Création d'une balle sous forme de disque se déplaçant dans un canevas.
    """
    def __init__(self, X, Y, R, c, a, b, f):
        self.x_centre = X
        # Les coordonnées du point supérieur gauche sont
        # (self.x_centre - self.rayon, self.y_centre - self.rayon)
        self.y_centre = Y
        # Les coordonnées du point inférieur droit sont
        # (self.x_centre + self.rayon, self.y_centre + self.rayon)
        self.rayon = R
        self.coul = c
        # Déplacement horizontale vers la droite si self.dx > 0
        self.dx = a
        # Déplacement verticale vers le bas si self.dy > 0
        self.dy = b
        # La balle ne se déplacera pas si self.flag est différent de 1
        self.flag = f
        self.balle = can.create_oval(self.x_centre - self.rayon,
                                     self.y_centre - self.rayon,
                                     self.x_centre + self.rayon,
                                     self.y_centre + self.rayon,
                                     fill=c)

    def mouvement(self):
        """Mouvement d'une balle."""
        if self.flag == 1:
            # Les coordonnées du centre sont changées
            self.x_centre = self.x_centre + self.dx
            self.y_centre = self.y_centre + self.dy
            # Nous changeons les coordonnées de l'objet de type Balle
            can.coords(self.balle, self.x_centre - self.rayon,
                       self.y_centre - self.rayon, self.x_centre + self.rayon,
                       self.y_centre + self.rayon)

    def bords(self):
        """Fait rebondir la balle sur les bords."""
        if not self.rayon < self.x_centre < L-self.rayon:
            self.dx = -(self.dx)
        if not self.rayon < self.y_centre < H-self.rayon:
            self.dy = -(self.dy)

    def change_dir(self):
        """Inverse la direction de la balle."""
        self.dx = -(self.dx)
        self.dy = -(self.dy)
        # Change la couleur de la balle car il y a eu collision.
        self.change_coul()

    def change_coul(self):
        """Change la couleur au hasard."""
        self.coul = rand.choice(liste_coul)
        can.itemconfig(self.balle, fill=self.coul)


class GroupeBalle:
    """
    Un groupe de balles. Il ne peut contenir que 5 balles.
    """
    def __init__(self):
        self.balles = []
        # Crée une copie pour le groupe des coordonnées de départ.
        self.copie_coord = list(coord_depart)

    def ajouter_balle(self):
        """Ajoute une balle dans le groupe."""
        coord_nb = rand.randint(0, len(self.copie_coord)-1)
        # Supprime le coordonnée pour le groupe car déjà utilisée.
        coord = self.copie_coord.pop(coord_nb)
        rayon = rand.randint(25, 75)
        self.balles.append(Balle(coord[0],
                                 coord[1],
                                 rayon,
                                 rand.choice(liste_coul),
                                 rand.choice(vitesses),
                                 rand.choice(vitesses),
                                 1))

    def lancer(self):
        """Lance les balles."""
        # Vérifie si le groupe n'a pas plus de 5 balles.
        assert len(self.balles) <= 5
        for o in self.balles:
            o.mouvement()
            o.bords()
        self.collisions()
        can.after(10, self.lancer)

    def pause(self):
        """Met en pause le groupe de balle."""
        if self.balles[0].flag == 1:
            for o in self.balles:
                o.flag = 0
        else:
            for o in self.balles:
                o.flag = 1

    def collisions(self):
        """Vérifie si il y a des collisions entre balles."""
        for i in range(len(self.balles)):
            # Retire la balle du groupe pour le comparait au autres balles.
            # Afin de ne pas comparer une balle avec elle-même.
            tmp = self.balles.pop(i)
            for e in self.balles:
                # Calcul la distance entre le centre des 2 balles.
                distance = abs(
                    math.sqrt(
                        (e.x_centre-tmp.x_centre)**2
                        + (e.y_centre-tmp.y_centre)**2
                        )
                    )
                if distance <= tmp.rayon + e.rayon:
                    tmp.change_dir()
            # Remet la balle dans le groupe.
            self.balles.insert(i, tmp)


fen = tk.Tk()

can = tk.Canvas(fen, width=L, height=H, bg='#000000')
can.pack(side=tk.BOTTOM)

# Créer le groupe G1 de balles.
G1 = GroupeBalle()
for i in range(5):
    G1.ajouter_balle()

bou_q = tk.Button(fen, text="Quitter", font="Arial 12 bold", command=fen.quit)
bou_q.pack(side=tk.RIGHT)

bou_l = tk.Button(fen, text="Lancer", font="Arial 12 bold", command=G1.lancer)
bou_l.pack(side=tk.LEFT)

bou_p = tk.Button(fen, text="Pause", font="Arial 12 bold", command=G1.pause)
bou_p.pack(side=tk.LEFT)

fen.mainloop()
