def suite(n: int) -> int:
    if n == 0:
        return 2
    valeurs = [2, 3]
    for i in range(2, n+1):
        valeurs += [(valeurs[1]/valeurs.pop(0))+4]
    return valeurs[1]


def suite_dyn(n: int) -> int:
    valeurs = [0]*(n+1)
    if n == 0:
        return 2
    valeurs[0] = 2
    valeurs[1] = 3
    for i in range(2, n+1):
        valeurs[i] = (valeurs[i-1]/valeurs[i-2]) + 4
    return valeurs[n]


def suite_rec(n: int) -> int:
    if n == 0:
        return 2
    elif n == 1:
        return 3
    else:
        return (suite_rec(n-1)/suite_rec(n-2)) + 4


def suite_rec_dyn(n: int) -> int:
    stock = [0]*(n+1)
    return res_suite(n, stock)


def res_suite(n: int, stock: [int]) -> int:
    if stock[n] != 0:
        return stock[n]
    elif n == 0:
        stock[n] = 2
        return stock[n]
    elif n == 1:
        stock[n] = 3
        return stock[n]
    else:
        stock[n] = (res_suite(n-1, stock)/res_suite(n-2, stock)) + 4
        return stock[n]


n = 5
assert suite(n) == suite_dyn(n) == suite_rec(n) == suite_rec_dyn(n) == 4.867532467532468
n = 0
assert suite(n) == suite_dyn(n) == suite_rec(n) == suite_rec_dyn(n) == 2
n = 1
assert suite(n) == suite_dyn(n) == suite_rec(n) == suite_rec_dyn(n) == 3
