class CompteBancaire:
    def __init__(self, nom: str, solde: int | float):
        self.nom = nom
        self.solde = solde

    def depot(self, somme: int | float):
        self.solde += somme

    def retrait(self, somme: int | float):
        self.solde -= somme

    def affiche(self):
        print('Le solde du compte bancaire de', self.nom,
              'est', self.solde, 'euros.')


compte1 = CompteBancaire('Dunan', 500)
compte1.depot(250)
compte1.retrait(50)
compte1.affiche()
