# Exercice 1

def recherche_min(tab: list) -> int:
    """Fonction qui renvoie l'indice de la premiere occurrence du minimum."""
    n = len(tab)
    mini = 0
    mini_valeur = tab[0]
    for i in range(1, n):
        if tab[i] < mini_valeur:
            mini = i
            mini_valeur = tab[i]
    return mini


assert recherche_min([5]) == 0
assert recherche_min([2, 4, 1]) == 2
assert recherche_min([5, 3, 2, 2, 4]) == 2

# Exercice 2


def separe(tab: list):
    gauche = 0
    droite = len(tab) - 1
    while gauche < droite:
        if tab[gauche] == 0:
            gauche = gauche + 1
        else:
            tab[gauche], tab[droite] = tab[droite], tab[gauche]
            droite = droite - 1
    return tab


assert separe([1, 0, 1, 0, 1, 0, 1, 0]) == [0, 0, 0, 0, 1, 1, 1, 1]
assert separe([1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0]) == [
    0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
