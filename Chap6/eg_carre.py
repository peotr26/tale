class Carre:
    def __init__(self, x: int):
        self.cote: int = x

    def perimetre(self):
        return 4*self.cote

    def aire(self):
        return self.cote**2


C1 = Carre(2)
assert C1.perimetre() == 8
assert C1.aire() == 4

C2 = Carre(5)
assert C2.perimetre() == 20
assert C2.aire() == 25
