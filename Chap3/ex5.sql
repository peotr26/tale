-- 1

SELECT Age
FROM Pilote
WHERE Matricule = 15;

-- 2

SELECT *
FROM Pilote;

-- 3

SELECT Type, Capacite
FROM Avion
ORDER BY Capacite DESC
WHERE Entrepot = 'Arras';

-- 4

SELECT Ville
FROM Pilote
WHERE (Salaire >8000 AND Age < 40);

-- 5

SELECT MAX(Salaire)
FROM Pilote;

-- 6

SELECT MAX(Salaire)
FROM Pilote
WHERE Age < 35;

-- 7

SELECT Num_avion, Entrepot
FROM Avion
ORDER BY Num_avion DESC
WHERE (Entrepot != 'Marolles-en-Hurepoix' AND Capacite > 200);

-- 8

SELECT AVG(Capacite)
FROM Avion
WHERE Entrepot = 'Calais';

-- 9

SELECT Nom
FROM Pilote
JOIN Vol, Avion
ON (Vol.Matricule = Pilote.Matricule AND Vol.Num_avion = Avion.Num_avion)
WHERE (Vol.Date_depart = 19951013 AND Avion.Capacite > 250);

-- 10

SELECT Nom
FROM Pilote
JOIN Vol
ON Vol.Matricule = Pilote.Matricule
WHERE Vol.Num_avion = 124;

-- 11

SELECT Nom
FROM Pilote
JOIN Avion
WHERE Ville = Avion.Num_avion;

--12

INSERT INTO Passager
VALUES (
    21,
    536023,
    'Rahim'
);

-- 13

UPDATE Passager
SET Nom_passager = 'Rohim'
WHERE Nom_passager = 'Rahim';
