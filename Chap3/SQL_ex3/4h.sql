SELECT DISTINCT Auteur.Nom_auteur, Auteur.Nom_pays, Pays.Population
FROM Auteur
JOIN Pays, Ecrire
ON (Pays.Nom_pays = Auteur.Nom_pays AND Auteur.Numero_SS = Ecrire.Numero_SS)
WHERE Ecrire.Nb_chapitres >= 30;