# Exercice 1

def max_dico(dico: dict) -> tuple:
    """
    Retourne l'abonné qui a le plus de like avec le nombre de like.'
    """
    dico_keys = list(dico.keys())
    dico_values = list(dico.values())
    maxi = dico_values[0]
    maxi_key = dico_keys[0]
    for i in range(1, len(dico)):
        if dico_values[i] > maxi:
            maxi = dico_values[i]
            maxi_key = dico_keys[i]
    return maxi_key, maxi


assert max_dico(
    {'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}
) == ('Ada', 201)
assert max_dico(
    {'Alan': 222, 'Ada': 201, 'Eve': 220, 'Tim': 50}
) == ('Alan', 222)

# Exercice 2


class Pile:
    """
    Classe définissant une structure de pile.
    """
    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """
        Renvoie le booléen True si la pile est vide, False sinon.
        """
        return self.contenu == []

    def empiler(self, v: int):
        """
        Place l’élément v au sommet de la pile.
        """
        self.contenu.append(v)

    def depiler(self) -> int:
        """
        Retire et renvoie l’élément placé au sommet de la pile,
        si la pile n’est pas vide.
        """
        if not self.est_vide():
            return self.contenu.pop()


def eval_expression(tab: list) -> int:
    """
    Renvoie le résultat de l'opération.
    """
    p = Pile()
    for element in tab:
        if element != '+' and element != '*':
            p.empiler(element)
        else:
            if element == '+':
                resultat = p.depiler() + p.depiler()
            else:
                resultat = p.depiler() * p.depiler()
            p.empiler(resultat)
    return p.depiler()


assert eval_expression([2, 3, '+', 5, '*']) == 25
assert eval_expression([8, 9, '*', 8, '+']) == 80
