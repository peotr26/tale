SELECT Nom_auteur, Auteur.Nom_pays, Pays.Population
FROM Auteur
JOIN Pays
ON Pays.Nom_pays = Auteur.Nom_pays
WHERE Pays.Population > 20
ORDER BY Nom_auteur DESC;