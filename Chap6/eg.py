import math
import time

# Eg 1


class Point:
    """
    Modélise un point du plan, avec une abscisse et une ordonnée.
    """
    def __init__(self, x, y):
        """Initialisation de la classe."""
        self.abscisse = x
        self.ordonnee = y

    def __repr__(self):
        """Retourne l'abscisse et l'ordonnée de l'objet."""
        return f"{self.abscisse},{self.ordonnee}"

    def affichage(self):
        """Affiche les coordonnées de l'objet."""
        print(self.abscisse, self.ordonnee)

    def distance(self, autre_point):
        resultat = math.sqrt((self.abscisse-autre_point.abscisse)**2
                             + (self.ordonnee-autre_point.ordonnee)**2)
        return resultat


# P1 = Point(5, 3)
# P2 = Point(-5, 6)
# print(P1)
# P2.affichage()
# print(P1.distance(P2))


# Eg 2


class PersonnageTerrien:
    """Modélisation d'un terrien."""
    def __init__(self, m: int, s: str, c: int, g: str):
        self.liste_sexes = ['F', 'H', 'I']
        self.liste_genres = ['Femme', 'Homme', 'Non-binaire']
        self.matricule = m
        self.sexe = s
        self.corpuale = c
        self.genre = g
        self.date_naissance = int(time.time())

    def transition_genre(self, g: str):
        self.genre = g


P1 = PersonnageTerrien(132, 'I', 9, 'Femme')
print('Le matricule est: ', P1.matricule)
print(P1.genre, end=' --> ')

P1.transition_genre('Non-binaire')
print(P1.genre)
