# Exercice 1

def compte_occurrences(x, tab: list) -> int:
    resultat = 0
    for e in tab:
        if e == x:
            resultat += 1
    return resultat


assert compte_occurrences(5, []) == 0
assert compte_occurrences(5, [-2, 3, 1, 5, 3, 7, 4]) == 1
assert compte_occurrences('a', ['a', 'b', 'c', 'a', 'd', 'e', 'a']) == 3

# Exercice 2

pieces = [1, 2, 5, 10, 20, 50, 100, 200]


def rendu_monnaie(somme_due, somme_versee):
    '''Renvoie la liste des pièces à rendre pour rendre la monnaie
    lorsqu'on doit rendre somme_versee - somme_due'''
    rendu = ...
    a_rendre = ...
    i = len(pieces) - 1
    while a_rendre > ...:
        while pieces[i] > a_rendre:
            i = i - 1
        rendu.append(...)
        a_rendre = ...
    return rendu
