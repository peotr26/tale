def e_fibonacci(n: int) -> int:
    '''Fonction classique qui rend l'élément n de la suite de Fibonacci.'''
    if n <= 1:
        return n
    a = 0
    b = 1
    for i in range(n-1):
        c = a+b
        a = b
        b = c
    return b


assert e_fibonacci(5) == 5
assert e_fibonacci(15) == 610


def e_fibonacci_rec(n: int) -> int:
    '''Fonction récursive qui rend l'élément n de la suite de Fibonacci.'''
    if n <= 1:
        return n
    else:
        return e_fibonacci_rec(n-1)+e_fibonacci_rec(n-2)


assert e_fibonacci_rec(5) == 5
assert e_fibonacci_rec(15) == 610
