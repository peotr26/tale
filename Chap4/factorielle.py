def factorielle(n: int) -> int:
    '''Fonction classique qui calcule la factorielle.'''
    resultat = 1
    for i in range(2, n+1):
        resultat *= i
    return resultat


assert factorielle(3) == 6
assert factorielle(4) == 24


def factorielle_recursive(n: int) -> int:
    '''Fonction récursive qui calcule la factorielle.'''
    if n == 0 or n == 1:
        return 1
    else:
        return factorielle_recursive(n-1)*n


assert factorielle_recursive(11) == factorielle(11)
assert factorielle_recursive(33) == factorielle(33)
