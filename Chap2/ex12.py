def positif(T: list) -> list:
    T2 = list(T)
    T3 = list()
    while T2 != []:
        x: int = T2.pop()
        if x >= 0:
            T3.append(x)
    T2 = []
    while T3 != []:
        x: int = T3.pop()
        T2.append(x)
    print('T = ', T)
    return T2


assert positif([-2, 0, 3, -5]) == [0, 3]
assert positif(([3, -5, -6, 9, 8, 4, -9])) == [3, 9, 8, 4]
