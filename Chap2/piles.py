def creer_pile_vide() -> list:
    '''Créer une pile vide.'''
    return list()


def est_vide(P: list) -> bool:
    '''Vérifie si la pile P est vide.'''
    return P == []


def empiler(P: list, e: int | float | str):
    '''Empile la valeur e dans la pile P.'''
    P += [e]


def depiler(P: list) -> int | float | str:
    '''Dépile la dernière valeur de la pile P.'''
    return P.pop()


def taille(P: list) -> int:
    '''Compte le nombre d'éléments de la pile P.'''
    compteur: int = 0
    Q: list = creer_pile_vide()
    while not est_vide(P):
        empiler(Q, depiler(P))
        compteur += 1
    while not est_vide(Q):
        empiler(P, depiler(Q))
    return compteur


def recherche(P: list, e: int | float | str) -> bool:
    '''Recherche l'élément e dans la pile P.'''
    Q: list = creer_pile_vide()
    marquage: bool = False
    while not est_vide(P) and marquage is False:
        tmp = depiler(P)
        empiler(Q, tmp)
        if tmp == e:
            marquage = True
    while not est_vide(Q):
        empiler(P, depiler(Q))
    return marquage
