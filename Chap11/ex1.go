package main

import (
	"fmt"
	"math/big"
)

func main() {
	tmp := suite(1000)
	fmt.Println(tmp)
}

func suite(n int) *big.Int {
	if n == 0 {
		return big.NewInt(0)
	}
	valeurs := []*big.Int{big.NewInt(0), big.NewInt(2)}
	for i := 2; i <= n; i++ {
		valeurs = append(valeurs[1:], operation(valeurs))
	}
	return valeurs[1]
}

func operation(suite []*big.Int) *big.Int {
	valeurs := [3]*big.Int{big.NewInt(0), big.NewInt(0), big.NewInt(0)}
	valeurs[0].Mul(suite[1], big.NewInt(2))
	valeurs[1].Mul(suite[0], big.NewInt(3))
	valeurs[2].Sub(valeurs[0], valeurs[1])
	valeurs[2].Sub(valeurs[2], big.NewInt(1))
	return valeurs[2]
}
