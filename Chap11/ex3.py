def coef_bin(n: int, k: int) -> int:
    if k == 0 or k == n:
        return 1
    else:
        return coef_bin(n-1, k) + coef_bin(n-1, k-1)


def coef_bin_dy(n: int, k: int, stock: {(int, int): int} = {}) -> int:
    if (n, k) in stock:
        return stock[(n, k)]
    elif k == 0 or k == n:
        stock[(n, k)] = 1
        return stock[(n, k)]
    else:
        stock[(n, k)] = coef_bin_dy(n-1, k, stock)+coef_bin_dy(n-1, k-1, stock)
        return stock[(n, k)]


assert coef_bin(5, 0) == coef_bin_dy(5, 0) == 1
assert coef_bin(5, 5) == coef_bin_dy(5, 5) == 1
assert coef_bin(5, 3) == coef_bin_dy(5, 3) == 10

print(coef_bin_dy(250, 100))
