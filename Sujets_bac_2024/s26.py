from random import randint

# Exercice 1


def ajoute_dictionnaires(d1: dict, d2: dict) -> dict:
    resultat = dict(d1)
    for e in d2:
        if e not in resultat:
            resultat[e] = d2[e]
        else:
            resultat[e] += d2[e]
    return resultat


assert ajoute_dictionnaires(
    {1: 5, 2: 7}, {2: 9, 3: 11}) == {1: 5, 2: 16, 3: 11}
assert ajoute_dictionnaires({}, {2: 9, 3: 11}) == {2: 9, 3: 11}
assert ajoute_dictionnaires({1: 5, 2: 7}, {}) == {1: 5, 2: 7}

# Exercice 2


def nombre_coups():
    '''Simule un jeu de plateau avec 12 cases et renvoie le nombre
    minimal de coups pour visiter toutes les cases.'''
    nombre_cases = 12
    # indique si une case a été vue
    cases_vues = [False] * nombre_cases
    nombre_cases_vues = 1
    cases_vues[0] = True
    case_en_cours = 0
    n = 0
    while nombre_cases_vues < 12:
        x = randint(1, 6)
        case_en_cours = (case_en_cours + x) % 12
        if cases_vues[case_en_cours] is False:
            cases_vues[case_en_cours] = True
            nombre_cases_vues = nombre_cases_vues + 1
        n = n + 1
    return n


# print(nombre_coups())
