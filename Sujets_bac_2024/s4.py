# Exercice 1

def recherche(tab: [int], n: int) -> int:
    """
    Renvoie l'indice de la dernière occurrence de l'entier n dans un tableau.
    Si l'entier n n'est pas dans le tableau, renvoie None.
    """
    occurence: int | None = None
    for i, e in enumerate(tab):
        if e == n:
            occurence = i
    return occurence


assert recherche([5, 3], 1) is None
assert recherche([2, 4], 2) == 0
assert recherche([2, 3, 5, 2, 4], 2) == 3

# Exercice 2


def distance_carre(point1: (int, int), point2: (int, int)) -> int:
    """ Calcule et renvoie la distance au carre entre
    deux points."""
    return (point1[0] - point2[0])**2 + (point1[1] - point2[1])**2


def point_le_plus_proche(depart: (int, int), tab: [(int, int)]) -> (int, int):
    """ Renvoie les coordonnées du premier point du tableau tab se
    trouvant à la plus courte distance du point depart."""
    min_point: (int, int) = tab[0]
    min_dist: int = distance_carre(depart, min_point)
    for i in range(1, len(tab)):
        if distance_carre(tab[i], depart) < min_dist:
            min_point = tab[i]
            min_dist = distance_carre(depart, min_point)
    return min_point


assert point_le_plus_proche((0, 0), [(7, 9), (2, 5), (5, 2)]) == (2, 5)
assert point_le_plus_proche((5, 2), [(7, 9), (2, 5), (5, 2)]) == (5, 2)
