# Exercice 1

def fibonacci(n: int) -> int:
    """
    Renvoie la n-ème valeur de la suite de Fibonacci.
    """
    n1, n2 = 0, 1
    for i in range(n-1):
        n1, n2 = n2, n1 + n2
    return n2


assert fibonacci(1) == 1
assert fibonacci(2) == 1
assert fibonacci(25) == 75025
assert fibonacci(45) == 1134903170

# Exercice 2


def pantheon(eleves: [str], notes: [int]) -> (int, [str]):
    """
    Renvoie un tuple contenant la meilleur note
    ainsi que la liste des élèves ayant eu cette note.
    """
    note_maxi = 0
    meilleurs_eleves = []
    for i in range(len(eleves)):
        if notes[i] == note_maxi:
            meilleurs_eleves.append(eleves[i])
        elif notes[i] > note_maxi:
            note_maxi = notes[i]
            meilleurs_eleves = [eleves[i]]
    return (note_maxi, meilleurs_eleves)


eleves_nsi = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
notes_nsi = [30, 40, 80, 60, 58, 80, 75, 80, 60, 24]
assert pantheon(eleves_nsi, notes_nsi) == (80, ["c", "f", "h"])

assert pantheon([], []) == (0, [])
