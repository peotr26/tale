T_ex = [3, 8, 9, 5, 11]

#  Recherche de l’existence d’une occurrence d’un élément e


def recherche_existe(T: list | str | tuple, e: int | float | str) -> bool:
    n = len(T)
    for i in range(0, n-1):
        if T[i] == e:
            return True
    return False


assert recherche_existe(T_ex, 9) is True

# Recherche de l’indice de la première occurrence d’un élément e


def recherche_occ(T: list | str | tuple, e: int | float | str) -> int | float | str:
    n = len(T)
    for i in range(0, n-1):
        if T[i] == e:
            return i
    return -1


assert recherche_occ(T_ex, 9) == 2

# Recherche du maximum


def recherche_maxi(T: list | tuple) -> int | float:
    n = len(T)
    maxi = T[0]
    for i in range(1, n):
        if T[i] > maxi:
            maxi = T[i]
    return maxi


assert recherche_maxi(T_ex) == 11

# Recherche de l’indice du maximum


def recherche_occ_maxi(T: list | tuple) -> int | float:
    n = len(T)
    maxi = T[0]
    indice = 0
    for i in range(1, n):
        if T[i] > maxi:
            maxi = T[i]
            indice = i
    return i


assert recherche_occ_maxi(T_ex) == 4

# Recherche du minimum


def recherche_mini(T: list | tuple) -> int | float:
    n = len(T)
    mini = T[0]
    for i in range(1, n):
        if T[i] < mini:
            mini = T[i]
    return mini


assert recherche_mini(T_ex) == 3

# Recherche de l’indice du minimum


def recherche_occ_mini(T: list | tuple) -> int | float:
    n = len(T)
    mini = T[0]
    indice = 0
    for i in range(1, n):
        if T[i] < mini:
            print or float or str(T[i])
            mini = T[i]
            indice = i
    return indice


assert recherche_occ_mini(T_ex) == 0

# Algorithme de recherche par dichotomie d’un élément e


def recherche_dicho(T: list | tuple, e: int | float) -> int | float:
    n = len(T)
    gauche = 0
    droite = n-1
    while gauche <= droite:
        milieu = (gauche+droite)//2
        if T[milieu] == e:
            return milieu
        elif T[milieu] < e:
            gauche = milieu+1
        else:
            droite = milieu-1
    return -1


assert recherche_dicho([3, 5, 8, 9, 11], 5) == 1

# Algorithme de tri par insertion


def tri_insertion(T: list) -> list:
    n = len(T)
    for i in range(1, n-1):
        tmp = T[i]
        j = 0
        while T[j] < T[i]:
            j = j+1
        for k in range(i-1, j-1, -1):
            T[k+1] = T[k]
        T[j] = tmp
    return T


assert tri_insertion(T_ex) == [3, 5, 8, 9, 11]

# Algorithme de tri par sélection


def tri_selection(T: list) -> list:
    n = len(T)
    for i in range(0, n-2):
        indice_mini = i
        for j in range(i+1, n-1):
            if T[j] < T[indice_mini]:
                indice_mini = j
        tmp = T[i]
        T[i] = T[indice_mini]
        T[indice_mini] = tmp
    return T


assert tri_selection(T_ex) == [3, 5, 8, 9, 11]

# Algorithmes gloutons


def rendu_monnaie_centimes(somme_a_rendre: int | float) -> list:
    pieces = [1, 2, 5, 10, 20, 50, 100, 200]
    rendu = []
    i = len(pieces) - 1
    while somme_a_rendre > 0:
        if pieces[i] <= somme_a_rendre:
            rendu.append(pieces[i])
            somme_a_rendre -= pieces[i]
        else:
            i -= 1
    return rendu


assert rendu_monnaie_centimes(15) == [10, 5]
