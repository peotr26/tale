# Exercice 1

def liste_puissances(a: int, n: int) -> [int]:
    resultat = [a]
    for i in range(n-1):
        resultat.append(resultat[-1]*a)
    return resultat


def liste_puissances_borne(a: int, borne: int) -> [int]:
    tmp = 1
    resultat = []
    while True:
        tmp = tmp*a
        if tmp < borne:
            resultat.append(tmp)
        else:
            return resultat


assert liste_puissances(3, 5) == [3, 9, 27, 81, 243]
assert liste_puissances(-2, 4) == [-2, 4, -8, 16]

assert liste_puissances_borne(2, 16) == [2, 4, 8]
assert liste_puissances_borne(5, 5) == []

# Exercice 2

dico = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6,
        "G": 7, "H": 8, "I": 9, "J": 10, "K": 11, "L": 12,
        "M": 13, "N": 14, "O": 15, "P": 16, "Q": 17,
        "R": 18, "S": 19, "T": 20, "U": 21, "V": 22,
        "W": 23, "X": 24, "Y": 25, "Z": 26}


def est_parfait(mot):
    # mot est une chaîne de caractères (en lettres majuscules)
    code_concatene = ""
    code_additionne = 0
    for c in mot:
        code_concatene = code_concatene + str(dico[c])
        code_additionne = code_additionne + dico[c]
    code_concatene = int(code_concatene)
    if code_concatene % code_additionne == 0:
        mot_est_parfait = True
    else:
        mot_est_parfait = False
    return code_additionne, code_concatene, mot_est_parfait


assert est_parfait("PAUL")[2] is False
assert est_parfait("ALAIN")[2] is True
