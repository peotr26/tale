# Not working, there seems to be an issue.

class Noeud:
    def __init__(self, cle: int):
        self.cle = cle
        self.gauche = None
        self.droite = None

    def insere(self, cle: int):
        if cle < self.cle:
            if self.gauche is None:
                self.gauche = Noeud(cle)
            else:
                self.gauche.insere(cle)
        elif cle > self.cle:
            if self.droite is None:
                self.droite = Noeud(cle)
            else:
                self.droite.insere(cle)

    def hauteur(self) -> int:
        if self.gauche is None and self.droite is None:
            return 1
        if self.gauche is None:
            return 1 + self.droite.hauteur()
        elif self.droite is None:
            return 1 + self.gauche.hauteur()
        else:
            hg = self.gauche.hauteur()
            hd = self.droite.hauteur()
            if hg > hd:
                return hg + 1
            else:
                return hd + 1

    def taille(self) -> int:
        if self.gauche is None and self.droite is None:
            return 1
        if self.gauche is None:
            return 1 + self.droite.taille()
        elif self.droite is None:
            return 1 + self.gauche.taille()
        else:
            return 1 + self.droite.taille() + self.gauche.taille()


class Arbre:
    def __init__(self, cle: int):
        self.racine = Noeud(cle)

    def insere(self, cle: int):
        self.racine.insere(cle)

    def hauteur(self) -> int:
        return self.racine.hauteur()

    def taille(self) -> int:
        return self.racine.taille()

    def bien_construit(self):
        t_max = 2**self.racine.hauteur()-1
        t_min = 2**(self.racine.hauteur()-1)-1
        return t_min < self.racine.taille() <= t_max


a = Arbre(10)

a.insere(20)
a.insere(15)
a.insere(12)
a.insere(8)
a.insere(4)
a.insere(5)

b = Arbre(10)

b.insere(5)
b.insere(4)
b.insere(8)
b.insere(15)
b.insere(12)
b.insere(20)

assert a.hauteur() == 4
assert a.taille() == 7
assert a.bien_construit() == False
assert b.bien_construit() == True
