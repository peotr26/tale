class Triangle:
    def __init__(self, a: int, b: int, c: int):
        self.a: int = a
        self.b: int = b
        self.c: int = c

    def est_rectangle(self):
        return self.a**2 + self.b**2 == self.c**2


T1: object = Triangle(3, 4, 5)
assert T1.est_rectangle() is True

T2: object = Triangle(7, 9, 15)
assert T2.est_rectangle() is False
