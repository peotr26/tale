# Exercice 1

def convertir(tab: list) -> int:
    '''
    tab est un tableau d'entiers, dont les éléments sont 0 ou 1,
    et représentant un entier écrit en binaire.
    Renvoie l'écriture décimale de l'entier positif dont la
    représentation binaire est donnée par le tableau tab
    '''
    resultat = 0
    for i in range(len(tab)):
        if tab[i] == 1:
            resultat += 2**(len(tab)-1-i)
    return resultat


assert convertir([1, 0, 1, 0, 0, 1, 1]) == 83
assert convertir([1, 0, 0, 0, 0, 0, 1, 0]) == 130

# Exercice 2


def tri_insertion(tab: list):
    '''
    Fonction qui trie une liste avec la méthode par insertion.
    '''
    n = len(tab)
    for i in range(1, n):
        valeur_insertion = tab[i]
        # Variable j est utilisée pour déterminer où placer la valeur à insérer
        j = i
        # Tant qu'on a pas trouvé la place de l'élément à insérer
        # On décale les valeurs du tableau vers la droite
        while j > 0 and valeur_insertion < tab[j-1]:
            tab[j] = tab[j-1]
            j = j-1
        tab[j] = valeur_insertion


liste = [9, 5, 8, 4, 0, 2, 7, 1, 10, 3, 6]
tri_insertion(liste)
print(liste)
