class Carte:
    def __init__(self, c: int, v: int):
        assert 0 <= c <= 3
        assert 1 <= v <= 13
        self.couleur = c
        self.valeur = v

    def getNom(self) -> str:
        if self.valeur > 1 and self.valeur < 11:
            return str(self.valeur)
        elif self.valeur == 11:
            return 'Valet'
        elif self.valeur == 12:
            return 'Dame'
        elif self.valeur == 13:
            return 'Roi'
        else:
            return 'As'

    def getCouleur(self) -> str:
        return ['pique', 'coeur', 'carreau', 'trefle'][self.couleur]


class PaquetDeCarte:
    def __init__(self):
        self.contenu = []

    def remplir(self):
        for i in range(4):
            for k in range(1, 14):
                self.contenu.append(Carte(i, k))

    def getCarteAt(self, pos: int):
        assert 0 <= pos <= 51
        print(
            self.contenu[pos].getNom(), 'de', self.contenu[pos].getCouleur()
            )
